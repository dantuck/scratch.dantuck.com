import { writable, readable, derived } from 'svelte/store';

export const time = readable(new Date(), function start(set) {
	const interval = setInterval(() => {
		set(new Date());
	}, 1000);

	return function stop() {
		clearInterval(interval);
	};
});

export const paused = writable(true);
export const seconds = writable(5);
export const start = writable(new Date());

export const elapsed = derived(
	[time, seconds, start, paused],
	([$time, $seconds, $start, $paused]) => {
        if ($paused) { /* empty */ } else {
            let currentCount = $seconds - Math.round(($time - $start) / 1000);
            if (currentCount < 0)
            {
                paused.set(true);
            }

            return currentCount > $seconds ? $seconds : currentCount;
        }

        return 0;
    }
);